<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

       
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        
         
        
        <title><?= $title?></title>
    </head>
    <body>
        <div class="container">
            <h1 class="text-primary"><?=$title?></h1>
            <form action="<?= site_url('productos/alta')?>" method="post">
                <div class="form-group">
                    <label for="CodigoProducto">CodigoProducto</label>
                    <input type="text" name="cp" id="cp" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" id="nombre" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="codigofamilia">CodigoFamilia</label>
                    <input type="text" name="cf" id="cf" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="caracteristicas">Características</label>
                    <input type="text" name="características" id="características" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="color">Color</label>
                    <input type="text" name="color" id="color" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="tipoIVA">TipoIVA</label>
                    <input type="text" name="tIVA" id="tIVA" class="form-control"/>
                </div>
                <input type="submit" name ="registrar" value="Registrar"/>
            </form> 
        </div>    
    </body>
</html>
        